const { Agent, get } = require('https')
const { createSecureServer } = require('http2')
const { TextDecoder } = require('util')
const { createDeflate, createGzip, createUnzip } = require('zlib')
const { PassThrough } = require('stream')
const { readFileSync } = require('fs')
const { URL, URLSearchParams } = require('url')
const _csrfToken = require('crypto').rng(6).toString('base64').replace(/\//g, '_').replace(/\+/g, '-')
const bi = this.BigInt || global.BigInt || this.String || global.String || BigInt || String
console.log(_csrfToken)
const h1o = {
	hostname: 'www.webnovel.com',
	port: 443,
	path: '/apiajax/chapter/',
	keepAlive: true,
	headers: {
		'Cookie': `_csrfToken=${_csrfToken}`,
		'Accept-Encoding': 'deflate, gzip'
	},
}
h1o.agent = new Agent(h1o)

const percent = 'Intl' in this ? new Intl.NumberFormat('en-AU', {
	style: 'percent',
	maximumFractionDigits: 2
}) : {format(v){return String(Math.floor(v * 1e4) / 100) + '%'}}

const wrapres = (req, res) => {
	let d = !0
		&& 'android' !== process.platform
		&& 'string' === typeof req.headers['accept-encoding']
		? req.headers['accept-encoding'].split(', ').sort().concat([null])
		: [null]
	for (const e of d) switch (e) {
		case 'deflate':
			let df = createDeflate()
			df.pipe(res)
			return {z: df, enc: 'deflate'}
		case 'gzip':
			let gz = createGzip()
			gz.pipe(res)
			return {z: gz, enc: 'gzip'}
		case null:
			let nz = new PassThrough
			nz.pipe(res)
			return {z: nz, enc: 'identity'}
		default:
			continue
	}
}
const unwrap = res => 'string' === typeof res.headers['content-encoding'] ? res.pipe(createUnzip()) : res.pipe(new PassThrough)
const bik = new Set(['bookId','chapterId','firstChapterId','nextChapterId','preChapterId','id'])
const erq = hs => new Promise((res, rej) => {
	let rq = get(hs, async rdd => {
		rdd.once('error', rej)
		let c = []
		await new Promise(r => unwrap(rdd)
			.on('readable', function onReadable() {
				let ddd = this.read()
				if (ddd) c.push(ddd)
			}).once('end', () => process.nextTick(r))
		)
		
		let t = new TextDecoder
		let d = c.reduce((s, b, i) => s + t.decode(b, {stream: i === c.length - 1}), '')
		try {
			let j = JSON.parse(d, (k, v) => bik.has(k) ? bi(v) : v)
			res(j.data)
		} catch (e) {
			e.extdata = {d, c}
			rej(e)
		}
	}).once('error', rej)
	rq.setTimeout(5e3, () => rq.abort())
})
const rpl = s => s.replace(/[<>]/g, m => `&#${m.codePointAt(0)};`)
const _style = `* { box-sizing: border-box; }
:root { background: #000; color: #eee; font: 18px/1.2 'Fira Sans','Noto Sans','Roboto',sans-serif; }
:root, body { margin: 0; }
article { padding: 6px; margin: auto; max-width: 60ch; hyphens: auto; text-align: justify; }
:link, :visited { color: lime; }
progress { display: block; width: 100%; }
aside, h1, h2, a { text-align: center; }
aside {
	background: #444;
	position: sticky;
	box-shadow: 0px 1px 5px 5px #444;
	top: 0;
	left: 0;
	width: 100%;
	height: 4rem;
}
h2 {
	background: #222;
	position: sticky;
	box-shadow: 0px 1px 5px 5px #222;
	top: 4.1rem;
	left: 0;
	width: 100%;
}
nav {
	flex-flow: row wrap;
	justify-content: space-around;
	display: flex;
	margin-bottom: 50px;
	box-shadow: 0px 0px 10px 10px #333
}
nav a {
	display: block;
	flex-grow: 1;
	border-left: 2px solid #eee;
	border-right: 2px solid #eee;
	padding: 8px;
	background: #333;
	color: #ddf;
}
li {
	margin-bottom: 1ch;
}
time {
	font-family: 'Fira Sans Condensed','Fira Sans',sans-serif;
	font-stretch: condensed;
}
:root:not(.js) .js {display: none;}`
const tmpl = ({title = 'Title', style = _style, script, body = '<h1>No body provided</h1>', post = ''}) => String.raw`<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<meta name=viewport content="width=device-width"/>
<meta name="theme-color" content="#000000"/>
<title>${rpl(title)}</title>
<style>${'\n' + style.trim() + '\n'}</style>
<script>
document.documentElement.classList.add('js')
${script ? script.trim() + '\n' : ''}</script>
<body>
${body.trim()}
</body>${post.trim() ? '\n' + post.trim() : ''}
</html>`

const chcr = j => tmpl({
	title: String.raw`${j.bookInfo.bookName} - Chapter ${j.chapterInfo.chapterIndex}: ${j.chapterInfo.chapterName}`,
	script: String.raw`requestAnimationFrame(function initcp() {
	let cp = document.getElementById('copy')
	if (!cp) return requestAnimationFrame(initcp)
	let canon = document.getElementById('canon')
	cp.addEventListener('click', e => {
		navigator.clipboard.writeText(canon.href)
			.then(console.log)
			.catch(e => alert('stack' in e ? e.stack : e))
	})
})`,
	body: String.raw`<aside>
<progress max=${j.bookInfo.totalChapterNum} value=${j.chapterInfo.chapterIndex}>
	${j.chapterInfo.chapterIndex} of ${j.bookInfo.totalChapterNum}
</progress>
<p>${percent.format(j.chapterInfo.chapterIndex / j.bookInfo.totalChapterNum)}, from ${j._from || 'network'}</p>
</aside>
<main>
<h1>${rpl(j.bookInfo.bookName)}</h1>
<h2><a id=canon rel="canonical external noopener noreferrer" target=_blank href="//www.webnovel.com/book/${j.bookInfo.bookId}/${j.chapterInfo.chapterId}">Chapter ${j.chapterInfo.chapterIndex}: ${rpl(j.chapterInfo.chapterName)}</a> <button class=js id=copy>copy</button></h2>
<article>
${j.chapterInfo.content.replace(/<\/?[^p\/]|[^p\/]>|<[^b\/][^rp]\/?>/g, m => rpl(m))}
</article>
</main>
<nav>
<a rel=prev href="/chapter/${j.bookInfo.bookId}/${j.chapterInfo.preChapterId}/">&lArr; Previous<br>${j.chapterInfo.chapterIndex-1}</a>
<a rel=index href="/book/${j.bookInfo.bookId}/">Index</a>
<a rel=next href="/chapter/${j.bookInfo.bookId}/${j.chapterInfo.nextChapterId}/">Next &rArr;<br>${j.chapterInfo.chapterIndex+1}</a>
</nav>`,
	post: String.raw`<link rel="prefetch prerender next" href="/chapter/${j.bookInfo.bookId}/${j.chapterInfo.nextChapterId}/" as="html" type="document"/>`
})
const chls = j => tmpl({
	title: j.bookInfo.bookName,
	body: String.raw`<aside>
<progress indeterminite></progress>
<p>from ${j._from || 'network'}</p>
</aside>
<main>
<h1>${rpl(j.bookInfo.bookName)}</h1>${
j.volumeItems.reduce((html, {index, name: vname, chapterCount, chapterItems}, i) => String.raw`${html}
<section>
<h2>Volume ${vname ? index + ' - ' + vname : index} (${chapterCount} chapters)</h2>
<article>
<ol>${chapterItems.reduce((dd, {id, index, name: cname}) => String.raw`${dd}
<li value=${index}><a href="/chapter/${j.bookInfo.bookId}/${id}/">${rpl(cname)}</a></li>`, '')}
</ol>
</section>
</article>`, '')}
</main>`
})
const prft = m => tmpl({
	title: 'List of currently cached content',
	script: String.raw`const k = []
k.kill = function kill() {
	let af
	while (af = this.shift()) cancelAnimationFrame(af)
	return this
}
k.start = (el, i) => {
	const d = new Date(el.dateTime || el.getAttribute('datetime')).getTime()
	const now = (Date.now() - performance.now()) - d
	let last = -1
	k[i] = requestAnimationFrame(function updateTime(ms) {
		const g = Math.round((now + ms) / 1000)
		if (last !== g) el.textContent = (g < 60)
			? g.toString() + '\u200As\u200A' + ' ago'
			: (g < 3600)
			? Math.floor(g / 60).toString() + '\u200Am\u200A' + (g % 60).toString() + '\u200As ago'
			: (g < 86400)
			? Math.floor(g / 3600).toString() + '\u200Ah\u200A' + Math.floor((g % 3600) / 60).toString() + '\u200Am\u200A' + (g % 60).toString() + '\u200As ago'
			: Math.floor(g / 86400).toString() + '\u200Ad\u200A' + Math.floor((g % 86400) / 3600).toString() + '\u200Ah\u200A' + Math.floor((g % 3600) / 60).toString() + '\u200Am\u200A' + (g % 60).toString() + '\u200As ago'
		last = g
		k[i] = requestAnimationFrame(updateTime)
	})
	return el
}
k.restart = function restart(els = document.querySelectorAll('time[datetime]')) {
	this.kill()
	for (let i = 0; i < els.length; i++) this.start(els[i], i)
	return this
}
window.addEventListener('DOMContentLoaded', () => k.restart())`,
	body: String.raw`<aside>
<progress value=100></progress>
<p>from cache list</p>
</aside>
<main>
${m.size ? `<article>
<dl>
${Array.from(m, ([k, v]) => `<dt>${k === bi(v.bookInfo.bookId) ? 'book' : v.bookInfo.bookName} (${k})</dt>
<dd>${'volumeItems' in v
	? `<a target=_blank rel=noopener href="/book/${k}/">${rpl(v.bookInfo.bookName)}</a>`
	: `<a target=_blank rel=noopener href="/chapter/${v.bookInfo.bookId}/${k}/">${rpl('chapterInfo' in v ? v.chapterInfo.chapterName : v.bookInfo.bookId + '/' + k)}</a>`
}, loaded <time datetime="${v._added.toISOString()}" title="${v._added.toUTCString()}">at ${v._added.toUTCString()}</time></dd>`).join('\n')}
</dl>
</article>`
: '<h2>No loaded content</h2>'}
</main>`
})

const prefetch = new Map
const getContent = async (h, id, hs) => 'no-cache' !== h.pragma && prefetch.has(id) && prefetch.get(id)
	? (console.log('\tfrom prefetch'), prefetch.get(id) || erq(hs))
	: (console.log('\tfrom networks'), erq(hs))

const clrstn = (id, content) => prefetch.size > 0xff && Date.now() - prefetch.get(id)._added.getTime() < 864e5
	? content._timeout = _prefetch.delete(id)
	: content._timeout = setTimeout(clrstn, 6e5, id, content)
const sctn = (id, content) => () => {
	content._from = 'cache'
	if (!prefetch.has(id))
		(prefetch.set(id, content)) && (content._added = new Date)
	else clearTimeout(content._timeout)

	content._timeout = setTimeout(clrstn, 36e5, id, content)
}
const dealt = ([s, ns], path, type) => console.log(
	'\tDealt with %s %s in %d.%ss',
	type,
	path,
	s,
	String(ns).padStart(9, '0')
)

//const ch = /^\/(?<type>[^\/]+|)(?:\/(?<bId>\d+)(?:\/(?<cId>\d*)\/?)?)?$/
const ch = /^\/([^\/]+|)(?:\/(\d+)(?:\/(\d*)\/?)?)?$/
createSecureServer({
	key: readFileSync('localhost-privkey.pem'),
	cert: readFileSync('localhost-cert.pem'),
	allowHTTP1: 'android' !== process.platform
}, async function onRequest (req, res) {
	let t = process.hrtime()
	const hs = {...h1o}
	console.count('rq')
	console.log('\t[%s] %s', new Date().toISOString(), req.url)
	let [, type, bId, cId] = new URL(req.url, 'https://localhost:8443/').pathname.match(ch)
	const bookId = bi(bId || -1),
		chapterId = bi(cId || -1)
	// console.log({bookId, chapterId, bId, cId, type})
	switch (type) {
		case 'chapter':
			chapter: {
				hs.path += 'GetContent?' + new URLSearchParams({bookId, chapterId, _csrfToken, _: Date.now()})

				const content = await getContent(req.headers, chapterId, hs)
				if ('prefetch' === req.headers.purpose) {
					res.writeHead(201, {'cache-control': 'max-age=0', 'content-length': '0'})
					res.end(sctn(chapterId, content))
					process.nextTick(dealt, process.hrtime(t), 'chap', 'pre')
					return
				}
				const {z, enc} = wrapres(req, res)
				try {
					const ctn = chcr(content)
					res.writeHead(200, {
						'content-type': 'text/html',
						'cache-control': 'max-age=86400',
						'content-encoding': enc,
						'content-length': Buffer.byteLength(ctn)
					})
					z.end(ctn, sctn(chapterId, content))

					process.nextTick(dealt, process.hrtime(t), 'chap', 'rdr')
					return 
				} catch (e) {
					if (!res.headersSent) res.writeHead(500, {
						'content-type': 'text/html',
						'content-encoding': enc
					})
					z.end('<pre>' + rpl(e.stack) + (e.extdata ? '\n' + JSON.stringify(e.extdata, null, 2) : '') + '</pre>')
					process.nextTick(dealt, process.hrtime(t), 'chap', 'err')
					return
				}
			}
		case 'book':
			index: {
				hs.path += 'GetChapterList?' + new URLSearchParams({bookId, _csrfToken, _: Date.now()})

				const content = await getContent(req.headers, bookId, hs)
				if ('prefetch' === req.headers.purpose) {
					res.writeHead(201, {'cache-control': 'max-age=0', 'content-length': '0'})
					res.end(sctn(bookId, content))
					process.nextTick(dealt, process.hrtime(t), 'book', 'pre')
					return
				}
				const {z, enc} = wrapres(req, res)
				try {
					const ctn = chls(content)
					res.writeHead(200, {
						'content-type': 'text/html',
						'cache-control': 'max-age=86400',
						'content-encoding': enc,
						'content-length': Buffer.byteLength(ctn)
					})
					z.end(ctn, sctn(bookId, content))

					process.nextTick(dealt, process.hrtime(t), 'book', 'rdr')
					return 
				} catch (e) {
					if (!res.headersSent) res.writeHead(500, {
						'content-type': 'text/html',
						'content-encoding': enc
					})
					z.end('<pre>' + rpl(e.stack) + (e.extdata ? '\n' + JSON.stringify(e.extdata, null, 2) : '') + '</pre>')
					process.nextTick(dealt, process.hrtime(t), 'book', 'err')
					return
				}
			}
		case '':
		case 'prefetched':
			rprefetch: {
				const {z, enc} = wrapres(req, res)
				try {
					const ctn = prft(prefetch)
					res.writeHead(200, {
						'content-type': 'text/html',
						'cache-control': 'max-age=0',
						'content-encoding': enc,
						'content-length': Buffer.byteLength(ctn)
					})
					z.end(ctn)

					process.nextTick(dealt, process.hrtime(t), 'prft', 'rdr')
					return 
				} catch (e) {
					if (!res.headersSent) res.writeHead(500, {
						'content-type': 'text/html',
						'content-encoding': enc
					})
					z.end('<pre>' + rpl(e.stack) + (e.extdata ? '\n' + JSON.stringify(e.extdata, null, 2) : '') + '</pre>')
					process.nextTick(dealt, process.hrtime(t), 'prft', 'err')
					return
				}
			}
		default: {
			res.writeHead(404)
			res.end()
			process.nextTick(dealt, process.hrtime(t), 'e404', 'err')
		}
	}

}).listen(8443, function() {
	console.log('listening on https://localhost:8443')
})
